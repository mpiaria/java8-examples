package com.principal.ris.annuity.examples;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Flattening {
	public static void main(String[] args) {
		System.out.println("Distinct Integers");
		System.out.println("-----------------");
		getSomeIntegers().stream()
				.flatMap(Collection::stream)
				.distinct()
				.sorted()
				.forEach(System.out::println);    // terminal operation

		System.out.println();
		System.out.println("Sum of Integers");
		System.out.println("---------------");
		System.out.println(
				getSomeIntegers().stream()
						.flatMap(Collection::stream)
						.distinct()
						.reduce(0, Integer::sum));
	}

	private static List<List<Integer>> getSomeIntegers() {
		return Arrays.asList(
				Arrays.asList(2, 4, 6, 8),
				Arrays.asList(1, 2, 3, 4, 5),
				Arrays.asList(3, 4, 5, 6, 7));
	}
}
