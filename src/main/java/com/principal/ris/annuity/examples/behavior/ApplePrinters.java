package com.principal.ris.annuity.examples.behavior;

import com.principal.ris.annuity.examples.domain.Apple;

public class ApplePrinters {
	public static void printColorThenWeight(Apple apple) {
		System.out.println(String.format("Color: '%s', Weight: %d", apple.getColor(), apple.getWeight()));
	}

	public static void printWeightThenColor(Apple apple) {
		System.out.println(String.format("Weight: %d, Color: %s", apple.getWeight(), apple.getColor()));
	}
}
