package com.principal.ris.annuity.examples.domain;

public class Apple {
	private final int weight;
	private final String color;

	public Apple(int weight, String color) {
		this.weight = weight;
		this.color = color;
	}

	public Integer getWeight() {
		return weight;
	}

	public String getColor() {
		return color;
	}
}
