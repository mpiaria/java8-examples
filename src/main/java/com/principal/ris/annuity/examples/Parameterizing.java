package com.principal.ris.annuity.examples;

import com.principal.ris.annuity.examples.behavior.ApplePredicates;
import com.principal.ris.annuity.examples.behavior.ApplePrinters;
import com.principal.ris.annuity.examples.domain.Apple;

import java.util.Arrays;
import java.util.List;

public class Parameterizing {
	public static void main(String[] args) {
		List<Apple> apples = Arrays.asList(
				new Apple(1, "Red"),
				new Apple(5, "Pink"),
				new Apple(6, "Green"));

		System.out.println("Heavy Green Apples");
		System.out.println("------------------");
		apples.stream()
				.filter(ApplePredicates::isHeavy)
				.filter(ApplePredicates::isGreen)
				.forEach(ApplePrinters::printColorThenWeight);

		System.out.println();
		System.out.println("Light Apples");
		System.out.println("------------");
		apples.stream()
				.filter(a -> !ApplePredicates.isHeavy(a))
				.forEach(ApplePrinters::printWeightThenColor);
	}
}
