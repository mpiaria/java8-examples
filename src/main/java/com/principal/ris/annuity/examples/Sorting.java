package com.principal.ris.annuity.examples;

import com.principal.ris.annuity.examples.domain.Apple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Sorting {
	@SuppressWarnings("Convert2Lambda")
	public static void main(String[] args) {
		/*
		 * Java 7 version
		 */
		List<Apple> apples = new ArrayList<>(3);
		apples.add(new Apple(10, "Red"));
		apples.add(new Apple(5, "Green"));
		apples.add(new Apple(2, "Pink"));

		// we must pass an instance of Comparator to sort.
		apples.sort(new Comparator<Apple>() {
			public int compare(Apple a1, Apple a2) {
				return a1.getWeight().compareTo(a2.getWeight());
			}
		});

		// One thread is used until this for loop completes
		System.out.println();
		System.out.println("Sorted Weights");
		System.out.println("--------------");
		for (Apple apple : apples) {
			System.out.println(apple.getWeight());
		}

		/*
		 * Java 8 version
		 */
		apples = Arrays.asList(
				new Apple(10, "Red"),
				new Apple(5, "Green"),
				new Apple(2, "Pink"));

		// behavior parameterization. In Java 8, Comparator is a FunctionalInterface.
		// we can pass a function that matches its signature instead of an instance of Comparator.
		apples.sort(Comparator.comparing(Apple::getWeight));

		System.out.println();
		System.out.println("Functional Weights");
		System.out.println("------------------");

		// stream processing. perhaps multiple threads are being used in parallel.
		apples.stream()
				.mapToInt(Apple::getWeight)
				.forEach(System.out::println);
	}
}
