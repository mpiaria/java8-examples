package com.principal.ris.annuity.examples;

import com.principal.ris.annuity.examples.domain.Dish;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Streaming {
	@SuppressWarnings("Convert2Lambda")
	public static void main(String[] args) {
		/*
		 * Java 7 version
		 */
		List<Dish> menu = new ArrayList<>(4);
		menu.add(new Dish(400, "Peanut Butter and Jelly Sandwich"));
		menu.add(new Dish(350, "Slice of Pizza"));
		menu.add(new Dish(375, "Bowl of Cereal"));
		menu.add(new Dish(725, "Burrito"));
		menu.add(new Dish(50, "Saltines"));
		menu.add(new Dish(75, "Mixed Vegetables"));

		List<Dish> lowCaloricDishes = new ArrayList<>(menu.size());
		for (Dish dish : menu) {
			if (dish.getCalories() < 400) {
				lowCaloricDishes.add(dish);
			}
		}

		lowCaloricDishes.sort(new Comparator<Dish>() {
			@Override
			public int compare(Dish o1, Dish o2) {
				return o1.getCalories().compareTo(o2.getCalories());
			}
		});

		// garbage variable. only used as an intermediate container.
		List<String> lowCaloricDishNames = new ArrayList<>(lowCaloricDishes.size());
		for (Dish dish : lowCaloricDishes) {
			lowCaloricDishNames.add(dish.getName());
		}

		System.out.println("Low Caloric Dish Names");
		System.out.println("----------------------");
		for (String name : lowCaloricDishNames) {
			System.out.println(name);
		}

		/*
		 * Java 8 version
		 */
		lowCaloricDishNames = menu.stream()
				.filter(d -> d.getCalories() < 400)
				.sorted(Comparator.comparingInt(Dish::getCalories))
				.map(Dish::getName)
				.collect(Collectors.toList());

		System.out.println();
		System.out.println("Functional Dish Names");
		System.out.println("---------------------");
		lowCaloricDishNames.forEach(System.out::println);
	}
}
