package com.principal.ris.annuity.examples.behavior;

import java.util.function.DoubleUnaryOperator;

public class Converters {
	public static double converter(double quantity, double factor, double baseline) {
		return quantity * factor + baseline;
	}

	public static DoubleUnaryOperator curriedConverter(double factor, double baseline) {
		return quantity -> quantity * factor + baseline;
	}
}
