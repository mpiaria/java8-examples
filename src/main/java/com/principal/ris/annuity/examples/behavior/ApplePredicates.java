package com.principal.ris.annuity.examples.behavior;

import com.principal.ris.annuity.examples.domain.Apple;

public class ApplePredicates {
	public static boolean isHeavy(Apple apple) {
		return apple.getWeight() >= 5;
	}

	public static boolean isGreen(Apple apple) {
		return apple.getColor().equalsIgnoreCase("green");
	}
}
