package com.principal.ris.annuity.examples;

import com.principal.ris.annuity.examples.behavior.Converters;

import java.util.function.DoubleUnaryOperator;

public class Converting {
	public static void main(String[] args) {
		final double km = 1.0;
		final double celsius = 0.0;
		System.out.println(String.format("%.1f km is %.4f miles", km, Converters.converter(km, 0.6214, 0)));

		DoubleUnaryOperator kilometersToMiles = Converters.curriedConverter(0.6214, 0);
		DoubleUnaryOperator celsiusToFahrenheit = Converters.curriedConverter(1.8, 32);
		System.out.println(String.format("%.1f km is %.4f miles", km, kilometersToMiles.applyAsDouble(km)));
		System.out.println(String.format("%.1f C is %.1f F", celsius, celsiusToFahrenheit.applyAsDouble(celsius)));
	}
}
